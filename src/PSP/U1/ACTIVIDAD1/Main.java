package PSP.U1.ACTIVIDAD1;

/*
 * Se pretende realizar un programa el cual tiene un menu que ofrecerá una serie de opciones para controlar los hilos.
 * En su arranque el programa creará tres ficheros. Cada fichero creará un fichero diferente Fichero1.txt, Fichero2.txt
 * y Fichero3.txt.Se valorará que los hilos tengan nombre y prioridad asignada para poder crear primero el Fichero1.txt,
 * luego Fichero2.txt y por último la Fichero3.txt todos en la misma carpeta del proyecto, y saquen un mensaje cada uno
 * cuando acabe su tarea. Además de crear los ficheros cada hilo debe escribir 100 lineas la frase
 * "Soy un hilo nombre_del_hilo, mi prioridad es prioridad_hilo y estoy escribiendo el fichero nombre_fichero".
 * Una vez creados los ficheros y los datos se ejecutarán los siguietnes procesos desde el menú:
 *
 * 1)Demonio que cuenta líneas. Este será el primer hilo que podemos lanzar, será un demonio que muestra cada 10
 * segundos un mensaje con el número de líneas que tiene cada fichero. Para ello estableceremos la propiedad del hilo
 * setDaemon(true) y lo controlaremos con un bucle do while.
 */
public class Main {
    public static void main(String[] args) {

        CargarFicheros f1 = new CargarFicheros();
        CargarFicheros f2 = new CargarFicheros();
        CargarFicheros f3 = new CargarFicheros();
        Thread hilo1 = new Thread(f1);
        Thread hilo2 = new Thread(f2);
        Thread hilo3 = new Thread(f3);

        hilo1.setPriority(Thread.MAX_PRIORITY);
        hilo1.setName("Prioridad1");
        hilo2.setPriority(Thread.NORM_PRIORITY);
        hilo2.setName("Prioridad2");
        hilo3.setPriority(Thread.MIN_PRIORITY);
        hilo3.setName("Prioridad3");

        hilo1.start();
        hilo2.start();
        hilo3.start();
    }
}
