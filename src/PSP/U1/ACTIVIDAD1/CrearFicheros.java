package PSP.U1.ACTIVIDAD1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CrearFicheros {

    CrearFicheros(String ruta, String nombre) throws IOException {
        File fichero = new File(ruta);
        if (!fichero.exists()) {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fichero));
            for (int i = 0; i < 100; i++) {
                bw.write("Soy un hilo, " + Thread.currentThread().getName() + "mi prioridad es " + Thread.currentThread().getPriority() + "y estoy escribiendo el fichero " +nombre);
            }
        }
    }
}
